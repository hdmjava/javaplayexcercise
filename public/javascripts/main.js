hljs.initHighlightingOnLoad();

$(function () {
  var $progressbar = $('.progress-bar'),
    $progressvalue = $('#progressvalue');

  $('.exercise .exercise-completed').on('click', function (e) {
    e.preventDefault();
    var $t = $(e.currentTarget);

    var $exercise = $t.closest('.exercise');
    $exercise.removeClass('panel-default').addClass('completed panel-success');

    $.post($t.attr('href'), null, function (data) {
      $progressbar.css({width: data + '%'});
      $progressvalue.text(parseFloat(data).toFixed(2) + '%');
    });
  });
});