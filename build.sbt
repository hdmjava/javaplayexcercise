import org.irundaia.sass.ForceScss

name := "webapplicationexercise"
version := "1.0"

scalaVersion := "2.11.8"

lazy val webapplicationexercise = (project in file(".")).enablePlugins(PlayJava, SbtWeb).settings {
  libraryDependencies ++= Seq(
    javaJpa,
    "org.hibernate" % "hibernate-entitymanager" % "5.1.0.Final",
    "com.adrianhurt" %% "play-bootstrap" % "1.0-P25-B3"
  )
}

// unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )
routesGenerator := InjectedRoutesGenerator