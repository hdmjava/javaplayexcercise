package controllers;

import models.Exercise;
import models.Unit;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import security.Role;
import security.Security;
import views.html.exercise_form;
import views.html.unit_show;

import javax.inject.Inject;
import java.util.List;

import static play.libs.Json.toJson;

public class ExerciseController extends Controller {
    private FormFactory formFactory;

    @Inject
    public ExerciseController(FormFactory formFactory) {
        this.formFactory = formFactory;
    }

    /*
        TEMPLATE-CALLS
     */

    @Transactional
    @Security.AuthenticatedRole
    public Result showExercises() {
        List<Exercise> exercises = Exercise.findAllSorted();
        User user = User.findByUsername(request().username());

        return ok(unit_show.render(new Unit("Alle Aufgaben", ""), user.calculateProgress(), exercises, user.getCompletedExercises()));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result newExercise() {
        Form<Exercise> form = formFactory.form(Exercise.class);
        form.data().put("unit", "");
        return ok(exercise_form.render("Aufgabe erstellen", form, Unit.findAllOrderByNr()));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result editExercise(long id) {
        Exercise exercise = Exercise.find.byId(id);
        Form<Exercise> form = formFactory.form(Exercise.class).fill(exercise);
        if(exercise.getUnit() != null) {
            form.data().put("unit", exercise.getUnit().getId() + "");
        } else {
            form.data().put("unit", "");
        }
        return ok(exercise_form.render("Aufgabe bearbeiten", form, Unit.findAllOrderByNr()));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result saveExercise() {
        Form<Exercise> form = formFactory.form(Exercise.class).bindFromRequest("title", "shortDescription", "description", "hint", "solution");
        Exercise exercise = form.get();

        if(form.field("nr").value().equals("")) {
            exercise.setNr(-1);
        } else {
            exercise.setNr(Long.parseLong(form.field("nr").value()));
        }

        if(form.field("unit").value().equals("")) {
            Http.Context.current().args.put("error", "Keine Unit gesetzt");
            return badRequest(exercise_form.render("Aufgabe bearbeiten", form, Unit.findAllOrderByNr()));
        }

        exercise.setUnit(
                Unit.find.byId(Long.parseLong(form.field("unit").value()))
        );

        if(form.field("id").value().equals("")) {
            exercise.save();
        } else {
            exercise.setId(Long.parseLong(form.field("id").value()));
            exercise.merge();
        }
        return redirect(routes.ExerciseController.showExercises());
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result removeExercise(long id) {
        Exercise exercise = Exercise.find.byId(id);
        if (exercise == null) {
            flash("error", "Fehler beim Löschen: die entsprechende Aufgabe existiert nicht");
            return redirect(routes.ExerciseController.showExercises());
        } else {
            exercise.delete();
        }

        return redirect(routes.ExerciseController.showExercises());
    }



    /*
        REST-CALLS
     */

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result getExercises() {
        List<Exercise> exercises = Exercise.find.all();

        if (exercises == null) {
            return noContent();
        }

        return ok(toJson(exercises));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result getExercisesByUnit(long id) {
        if (Unit.find.byId(id) == null) {
            return notFound();
        }
        List<Exercise> exercises = Exercise.findByUnit(id);

        if (exercises.size() == 0) {
            return noContent();
        }

        return ok(toJson(exercises));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result getExercise(long id) {
        Exercise exercise = Exercise.find.byId(id);

        if (exercise == null) {
            return notFound();
        }
        return ok(toJson(exercise));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result createExercise() {
        Form<Exercise> exerciseForm = formFactory.form(Exercise.class).bindFromRequest("title", "shortDescription", "description", "hint", "solution");
        Exercise exercise = exerciseForm.get();

        if (!loadUnit(exercise)){
            return notFound("Unit not found");
        }

        exercise.save();
        return created(toJson(exercise));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result updateExercise(long id) {
        Exercise updatedExercise = formFactory.form(Exercise.class).bindFromRequest("title", "shortDescription", "description", "hint", "solution").get();

        Exercise exercise = Exercise.find.byId(id);
        if (exercise == null) {
            return notFound("Exercise not found");
        }

        if(updatedExercise.getTitle() != null) {
            exercise.setTitle(updatedExercise.getTitle());
        }
        if(updatedExercise.getDescription() != null) {
            exercise.setDescription(updatedExercise.getDescription());
        }
        if(updatedExercise.getHint() != null){
            exercise.setHint(updatedExercise.getHint());
        }
        if(updatedExercise.getSolution() != null){
            exercise.setSolution(updatedExercise.getSolution());
        }
        if(request().body().asJson().has("nr")) {
            exercise.setNr(request().body().asJson().get("nr").asLong());
        }
        if (!loadUnit(exercise)){
            return notFound("Unit not found");
        }

        exercise.update();
        return ok(toJson(exercise));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result deleteExercise(long id) {
        Exercise exercise = Exercise.find.byId(id);
        if (exercise == null) {
            return notFound("Exercise not found");
        }
        exercise.delete();
        return ok();
    }

    private boolean loadUnit(Exercise exercise) {
        if (request().body().asJson().has("unit")) {
            Unit unit = Unit.find.byId(request().body().asJson().get("unit").asLong());
            if (unit == null) {
                return false;
            }
            exercise.setUnit(unit);
        }
        return true;
    }
}
