package controllers;

import com.google.inject.Inject;
import models.Exercise;
import models.Unit;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import security.Role;
import security.Security;
import views.html.success;
import views.html.unit_form;

import java.util.List;

import static play.libs.Json.toJson;

public class UnitController extends Controller {
    private final FormFactory formFactory;

    @Inject
    public UnitController(FormFactory formFactory) {
        this.formFactory = formFactory;
    }

    /*
        TEMPLATE-CALLS
     */

    @Transactional
    @Security.AuthenticatedRole
    public Result showUnit(long nr) {
        User user = User.findByUsername(request().username());
        Unit unit = Unit.findByNr(nr);

        if(unit == null){
            if (nr>0){
                return ok(success.render());
            }
            return redirect(routes.UnitController.showUnits());
        }

        List<Exercise> exercises = Exercise.findByUnit(unit.getId());
        return ok(views.html.unit_show.render(unit, user.calculateProgress(), exercises, user.getCompletedExercises()));
    }

    @Transactional
    @Security.AuthenticatedRole
    public Result showUnits() {
        List<Unit> units = Unit.findAllOrderByNr();

        return ok(views.html.units.render(units));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result newUnit() {
        List<Unit> units = Unit.find.all();
        return ok(unit_form.render("Aufgabe erstellen", formFactory.form(Unit.class), units));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result editUnit(long id) {
        Unit unit = Unit.find.byId(id);
        List<Unit> units = Unit.find.all();
        return ok(unit_form.render("Unit bearbeiten", formFactory.form(Unit.class).fill(unit), units));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result saveUnit() {
        if(request().body().asFormUrlEncoded().get("unitNr")[0].equals("")) {
            request().body().asFormUrlEncoded().get("unitNr")[0] = "-1";
        }
        Form<Unit> form = formFactory.form(Unit.class).bindFromRequest("name", "description", "unitNr");
        Unit unit = form.get();

        if(form.field("id").value().equals("")) {
            unit.save();
        } else {
            unit.setId(Long.parseLong(form.field("id").value()));
            unit.merge();
        }
        return redirect(routes.UnitController.showUnits());
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result removeUnit(long id) {
        Unit unit = Unit.find.byId(id);
        unit.delete();
        return redirect(routes.UnitController.showUnits());
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result updateExerciseOrder(long unitId) {
        Unit unit = Unit.find.byId(unitId);

        List<Exercise> exercises;
        if (unitId != 0) {
            exercises = Exercise.findByUnit(unitId);
        } else {
            exercises = Exercise.find.all();
        }

        for(int i=0; i<exercises.size(); i++) {
            Exercise exercise = exercises.get(i);
            exercise.setNr(Long.parseLong(request().body().asFormUrlEncoded().get("order_" + exercise.getId())[0]));
            exercise.save();
        }

        if (unitId == 0) {
            return redirect(routes.ExerciseController.showExercises());
        }
        return redirect(routes.UnitController.showUnit(unit.getUnitNr()));
    }


    /*
        REST-CALLS
     */

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result getUnits() {
        List<Unit> units = Unit.find.all();

        if (units == null) {
            return noContent();
        }
        return ok(toJson(units));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result createUnit() {
        Unit unit = formFactory.form(Unit.class).bindFromRequest().get();
        unit.save();
        return created(toJson(unit));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result getUnit(long id) {
        Unit unit = Unit.find.byId(id);

        if (unit == null) {
            return notFound();
        }
        return ok(toJson(unit));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result updateUnit(long id) {
        Unit unit = formFactory.form(Unit.class).bindFromRequest().get();
        unit.setId(id);

        unit.merge();

        return ok(toJson(unit));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result deleteUnit(long id) {
        Unit unit = Unit.find.byId(id);

        if (unit == null) {
            return notFound();
        }

        unit.delete();
        return ok();
    }
}
