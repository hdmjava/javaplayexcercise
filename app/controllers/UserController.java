package controllers;

import models.Exercise;
import models.User;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import security.Role;
import security.Security;

import javax.inject.Inject;
import java.util.List;

import static play.libs.Json.toJson;

public class UserController extends Controller {
    private final FormFactory formFactory;

    @Inject
    public UserController(FormFactory formFactory) {
        this.formFactory = formFactory;
    }


    /*
        REST-CALLS
     */

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result getUsers() {
        List<User> users = User.find.all();
        return ok(toJson(users));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result createUser() {
        User user = formFactory.form(User.class).bindFromRequest().get();

        user.save();
        return created(toJson(user));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result getUser(long id){
        User user = User.find.byId(id);
        if (user == null){
            return notFound();
        }
        return ok(toJson(user));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result updateUser(long id) {
        User user = User.find.byId(id);
        if (user == null){
            return notFound();
        }

        User updatedUser = formFactory.form(User.class).bindFromRequest().get();

        if(updatedUser.getFirstName() != null) {
            user.setFirstName(updatedUser.getFirstName());
        }
        if(updatedUser.getLastName() != null) {
            user.setLastName(updatedUser.getLastName());
        }
        if(updatedUser.getPassword() != null) {
            user.setPassword(updatedUser.getPassword());
        }
        if(updatedUser.getEmail() != null) {
            user.setEmail(updatedUser.getEmail());
        }
        if(updatedUser.getRoles() != null) {
            user.setRoles(updatedUser.getRoles()); // TODO: security ;)
        }

        user.update();
        return ok(toJson(user));
    }

    @Transactional
    @Security.AuthenticatedRole(Role.ADMIN)
    public Result deleteUser(long id) {
        User user = User.find.byId(id);
        if (user == null) {
            return notFound();
        }

        user.delete();
        return ok();
    }

    @Security.AuthenticatedRole
    @Transactional
    public Result addCompletedExercise(long exerciseId) {
        User user = User.findByUsername(request().username());
        Exercise exercise = Exercise.find.byId(exerciseId);

        if (user == null) {
            return notFound("User not found");
        }
        if (exercise == null) {
            return notFound("Exercise not found");
        }

        user.addCompletedExcercise(exercise);
        user.save();

        return ok((user.getCompletedExercises().size()/(double)Exercise.getTotalCount())*100.0+"");
    }

    @Security.AuthenticatedRole
    @Transactional
    public Result removeCompletedExercise(long exerciseId) {
        User user = User.findByUsername(request().username());
        Exercise exercise = Exercise.find.byId(exerciseId);

        if (user == null) {
            return notFound("User not found");
        }
        if (exercise == null) {
            return notFound("Exercise not found");
        }

        user.removeCompletedExercise(exercise);
        user.save();

        return ok((user.getCompletedExercises().size()/(double)Exercise.getTotalCount())*100.0+"");
    }
}