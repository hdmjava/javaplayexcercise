package controllers;

import models.User;
import models.UserForm;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import security.Role;
import views.html.login;


public class SecurityController extends Controller {

    private final FormFactory formFactory;

    @javax.inject.Inject
    public SecurityController(FormFactory formFactory) {
        this.formFactory = formFactory;
    }

    public Result loginForm() {
        return ok(login.render(formFactory.form(UserForm.class), formFactory.form(UserForm.class)));
    }

    @Transactional
    public Result register() {
        Form<UserForm> form = formFactory.form(UserForm.class).bindFromRequest();
        UserForm userForm = form.get();

        if(User.findByUsername(userForm.getUsername()) != null) {
            flash("error", "Ein Nutzer mit diesem Namen existiert bereits!");
            return redirect(routes.SecurityController.loginForm());
        }

        User user = new User(userForm.getUsername(), User.hashPassword(userForm.getPassword()));
        user.save();

        updateSession(user);

        return redirect(routes.UnitController.showUnits());
    }

    @Transactional
    public Result login() {
        Form<UserForm> form = formFactory.form(UserForm.class).bindFromRequest();

        if(form.hasErrors()) {
            return badRequest(login.render(form, formFactory.form(UserForm.class)));
        }

        UserForm userForm = form.get();

        User user = User.findByUsernameAndPassword( userForm.getUsername(), User.hashPassword(userForm.getPassword()) );
        if (user != null) {
            updateSession(user);
            return redirect(routes.UnitController.showUnits());
        }

        session().clear();
        Http.Context.current().args.put("error", "Falscher Nutzername oder falsches Passwort!");
        return badRequest(login.render(form, formFactory.form(UserForm.class)));
    }

    public Result logout() {
        session().clear();
        return redirect(routes.Application.index());
    }

    private void updateSession(User user) {
        session().clear();
        session("userid", user.getId()+"");
        if(user.hasRole(Role.ADMIN)) {
            session("isAdmin", "true");
        }
    }

}
