package modules;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import models.Exercise;
import models.Unit;
import models.User;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import security.Role;

import java.util.logging.Logger;

public class Bootstrap extends AbstractModule {

    @Override
    protected void configure() {
        bind(BootstrapDummyData.class).asEagerSingleton();
    }

    public static class BootstrapDummyData {
        JPAApi jpa;

        @Inject
        public BootstrapDummyData(JPAApi jpa) {
            this.jpa = jpa;
            Logger.getLogger("EWA").info("Creating dummy data...");

            jpa.withTransaction((em) -> {
                JPA.bindForSync(em);

                if(User.find.all().size() > 0) {
                    return null;
                }

                User user = new User("test", User.hashPassword("test"));
                user.addRole(Role.ADMIN);
                user.save();

                Unit unit = new Unit("Unit 1: Datenbank-Model", "Wir lernen, wie wir ein Model über ORM via Hibernate anlegen können", 1);
                unit.save();

                new Exercise(
                        "Das User-Model",
                        "Wir legen ein User-Model als Entity an...",
                        "Erstelle eine Klasse \"Exercise\" im Package \"models\" die von Model erbt und annotiere sie als Entity. Die Klasse soll außerdem folgende öffentlichen Eigenschaften besitzen: String title, String description.",
                        "Annotationen werden in Java mit dem @-Zeichen markiert.",
                        "[code]@Entity\n" +
                                "public class Exercise extends Model {\n" +
                                "\n" +
                                "    @Id\n" +
                                "    @GeneratedValue(strategy = GenerationType.SEQUENCE)\n" +
                                "    private long id;\n" +
                                "    private String title;\n" +
                                "    private String shortDescription;\n" +
                                "    @Lob private String description;\n" +
                                "    @Lob private String hint;\n" +
                                "    @Lob private String solution;\n" +
                                "}[/code]",
                        1,
                        unit
                ).save();

                return null;
            });
        }
    }
}
