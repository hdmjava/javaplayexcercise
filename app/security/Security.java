package security;

import models.User;
import play.db.jpa.JPAApi;
import play.inject.Injector;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

import javax.inject.Inject;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Results.redirect;

public class Security {

    @With(AuthenticatedRoleAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface AuthenticatedRole {
        Role value() default Role.USER;
        Class<? extends RoleAuthenticator> authenticator() default DefaultRoleAuthenticator.class;
    }

    static class AuthenticatedRoleAction extends Action<AuthenticatedRole> {
        private final Injector injector;

        @Inject
        public AuthenticatedRoleAction(Injector injector) {
            this.injector = injector;
        }

        public CompletionStage<Result> call(final Http.Context ctx) {
            RoleAuthenticator authenticator = injector.instanceOf(configuration.authenticator());
            String username = authenticator.getAuthorizedUsername(ctx, configuration);
            if(username != null){
                Http.Context newctx = ctx.withRequest(ctx.request().withUsername(username));
                return delegate.call(newctx);
            }

            Result unauthorized = authenticator.onUnauthorized(ctx);
            return CompletableFuture.completedFuture(unauthorized);
        }
    }

    interface RoleAuthenticator {
        Result onUnauthorized(Http.Context ctx);
        String getAuthorizedUsername(Http.Context ctx, AuthenticatedRole configuration);
    }

    @SuppressWarnings("unused")
    static class DefaultRoleAuthenticator implements RoleAuthenticator {
        private final JPAApi jpa;

        @Inject
        public DefaultRoleAuthenticator(JPAApi jpa){
            this.jpa = jpa;
        }

        public Result onUnauthorized(Http.Context ctx) {
            ctx.flash().put("error", "Du hast nicht die erforderlichen Berechtigungen!");
            return redirect(controllers.routes.SecurityController.login());
        }

        @Override
        public String getAuthorizedUsername(Http.Context ctx, AuthenticatedRole configuration) {
            String sessionUserId = ctx.session().get("userid");

            if (sessionUserId == null) {
                return null;
            }

            User user = jpa.withTransaction(entityManager -> {
                return User.find.byId(Long.parseLong(sessionUserId));
            });

            if (user != null && ( configuration.value() == Role.USER || user.hasRole(configuration.value() ))) {
                return user.getUsername();
            }

            return null;
        }
    }
}
