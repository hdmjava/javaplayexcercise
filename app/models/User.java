package models;

import models.utils.Finder;
import models.utils.Model;
import play.db.jpa.JPA;
import security.Role;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Entity
public class User extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "userRoles" )
    private List<Role> roles = new ArrayList<Role>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "completedExercises")
    private Set<Exercise> completedExercises = new HashSet<>();

    public static Finder<Long, User> find = new Finder<>(User.class);

    public static User findByUsernameAndPassword(String username, String password) {
        TypedQuery<User> query = JPA.em().createQuery("select u from User u where u.username like :username and u.password like :password", User.class);
        query.setParameter("username", username);
        query.setParameter("password", password);

        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public static User findByUsername(String username) {
        TypedQuery<User> query = JPA.em().createQuery("select u from User u where u.username like :username", User.class);
        query.setParameter("username", username);

        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public static String hashPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes("UTF-8"));
            password = Base64.getEncoder().encodeToString(md.digest());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return password;
    }

    /**
     * default constructor
     */
    public User() {}

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public double calculateProgress() {
        int completedExercises = 0;
        if(getCompletedExercises() != null){
            completedExercises = getCompletedExercises().size();
        }
        return (completedExercises/(double)Exercise.getTotalCount())*100.0;
    }


    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Exercise> getCompletedExercises() {
        return completedExercises;
    }

    public void addCompletedExcercise(Exercise exercise) {
        completedExercises.add(exercise);
    }

    public boolean removeCompletedExercise(Exercise exercise) {
        return completedExercises.remove(exercise);
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role) {
        roles.add(role);
    }

    public boolean removeRole(Role role) {
        return roles.remove(role);
    }

    public boolean hasRole(Role role) {
        return roles.contains(role);
    }

    @Override
    public String toString() {
        String out = "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", completedExercises=[ ";

        if(completedExercises != null) {
            boolean first = true;
            for (Exercise e : completedExercises) {
                out += (first ? "{" : ", {") + e.getId() + ": " + e.getTitle() + "}";
                first = false;
            }
        }

        out += " ]" +
                '}';

        return out;
    }
}