package models;

import play.data.validation.Constraints;

public class UserForm {
    public UserForm() {}

    @Constraints.Required
    @Constraints.MinLength(3)
    protected String username;

    @Constraints.Required
    @Constraints.MinLength(4)
    protected String password;

    public void setUsername(String email) {
        this.username = email;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserForm{");
        sb.append("username='").append(username).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
