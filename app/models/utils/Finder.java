package models.utils;

import play.db.jpa.JPA;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class Finder<T, U> {
    private final Class<U> type;

    public Finder(Class<U> type) {
        this.type = type;
    }

    public U byId(T id) {
        return JPA.em().find(type, id);
    }

    public List<U> all() {
        CriteriaBuilder cb = JPA.em().getCriteriaBuilder();
        CriteriaQuery<U> cq = cb.createQuery(type);
        Root<U> entity = cq.from(type);
        cq.select(entity);

        TypedQuery<U> q = JPA.em().createQuery(cq);
        return q.getResultList();
    }
}
