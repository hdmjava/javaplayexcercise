package models.utils;

import play.db.jpa.JPA;

public abstract class Model {

    public void save() {
        JPA.em().persist(this);
    }

    public void delete() {
        JPA.em().remove(this);
    }

    public void update() {
        JPA.em().persist(this);
    }

    public void refresh() {
        JPA.em().refresh(this);
    }

    public void merge() { JPA.em().merge(this); }
}
