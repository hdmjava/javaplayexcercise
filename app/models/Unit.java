package models;

import models.utils.Finder;
import models.utils.Model;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.List;

@Entity
public class Unit extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String name;
    private String description;
    private long unitNr = -1;

    public static Finder<Long, Unit> find = new Finder<>(Unit.class);

    public static Unit findByNr(long nr) {
        TypedQuery<Unit> query = JPA.em().createQuery("select u from Unit u where u.unitNr=:nr", Unit.class);
        query.setParameter("nr", nr);
        try {
            return query.getSingleResult();
        } catch(NoResultException e) {
            return null;
        }
    }

    public static List<Unit> findAllOrderByNr() {
        TypedQuery<Unit> query = JPA.em().createQuery("select u from Unit u order by u.unitNr", Unit.class);
        return query.getResultList();
    }

    public Unit() {
    }

    public Unit(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Unit(String name, String description, long unitNr) {
        this.name = name;
        this.description = description;
        this.unitNr = unitNr;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getUnitNr() {
        return unitNr;
    }

    public void setUnitNr(long order) {
        this.unitNr = order;
    }

    @Override
    public String toString() {
        return "Unit{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Unit)) return false;

        Unit unit = (Unit) o;

        return id == unit.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
