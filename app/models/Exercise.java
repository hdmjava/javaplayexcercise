package models;

import models.utils.Finder;
import models.utils.Model;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.List;

@Entity
public class Exercise extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private String title;
    private String shortDescription;
    @Lob private String description;
    @Lob private String hint;
    @Lob private String solution;
    private long nr = -1;

    @ManyToOne
    private User creator;

    @ManyToOne
    private Unit unit;

    public static Finder<Long, Exercise> find = new Finder<>(Exercise.class);

    public static List<Exercise> findByUnit(long id) {
        TypedQuery<Exercise> query = JPA.em().createQuery("select e from Exercise e where e.unit.id=:id order by e.nr", Exercise.class);
        query.setParameter("id", id);
        return query.getResultList();
    }

    public static List<Exercise> findAllSorted() {
        TypedQuery<Exercise> query = JPA.em().createQuery("select e from Exercise e order by e.unit.unitNr, e.nr", Exercise.class);
        return query.getResultList();
    }

    public static long getTotalCount() {
        return JPA.em().createQuery("select count(e) from Exercise e", Long.class).getSingleResult();
    }

    /**
     * default constructor
     */
    public Exercise() {}

    public Exercise(String title, String shortDescription, String description, String hint, String solution, int nr, Unit unit) {
        this.title = title;
        this.shortDescription = shortDescription;
        this.description = description;
        this.hint = hint;
        this.solution = solution;
        this.nr = nr;
        this.unit = unit;
    }

    @Override
    public void delete(){
        Query query = JPA.em().createQuery("select u from User u where :exercise member of u.completedExercises", User.class);
        query.setParameter("exercise", this);

        for(User u:(List<User>) query.getResultList()){
            u.removeCompletedExercise(this);
            u.save();
        }

        super.delete();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public long getNr() {
        return nr;
    }

    public void setNr(long nr) {
        this.nr = nr;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Exercise{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", description='" + description + '\'' +
                ", hint='" + hint + '\'' +
                ", solution='" + solution + '\'' +
                ", creator=" + creator +
                ", unit=" + unit +
                '}';
    }
}
