package views.utils;

import com.google.common.html.HtmlEscapers;
import play.mvc.Http;

import java.util.regex.Pattern;

import static play.mvc.Http.Context.Implicit.flash;

public class ViewUtils {

    public static String filterHtml(String str) {
        str = HtmlEscapers.htmlEscaper().escape(str);
        System.out.println(str);

        Pattern p = Pattern.compile("\\[code([^]]*)](.+?(?=\\[/code])*)\\[/code]",
                Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

        str = p.matcher(str).replaceAll("<pre><code$1>$2</code></pre>");
        str = str.replaceAll("\\r?\\n", "<br />");

        p = Pattern.compile("\\[b](.+?(?=\\[/b])*)\\[/b]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        str = p.matcher(str).replaceAll("<strong>$1</strong>");

        p = Pattern.compile("\\[i](.+?(?=\\[/i])*)\\[/i]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        str = p.matcher(str).replaceAll("<em>$1</em>");

        p = Pattern.compile("\\[u](.+?(?=\\[/u])*)\\[/u]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        str = p.matcher(str).replaceAll("<u>$1</u>");

        p = Pattern.compile("\\[s](.+?(?=\\[/s])*)\\[/s]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        str = p.matcher(str).replaceAll("<strike>$1</strike>");

        p = Pattern.compile("\\[span\\s*(?:class=&quot;(.+?(?=&quot;)*)&quot;)?](.+?(?=\\[/span])*)\\[/span]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        str = p.matcher(str).replaceAll("<span class=\"$1\">$2</span>");

        return str;
    }

    public static String getErrorMessage() {
        if (Http.Context.current().args.containsKey("error")) {
            return Http.Context.current().args.get("error").toString();
        }
        if (flash().containsKey("error")) {
            return flash().get("error");
        }

        return null;
    }

    public static String getSuccessMessage() {
        if (Http.Context.current().args.containsKey("success")) {
            return Http.Context.current().args.get("success").toString();
        }
        if (flash().containsKey("success")) {
            return flash().get("success");
        }

        return null;
    }
}
